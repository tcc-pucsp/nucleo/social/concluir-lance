package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class ConcluirLanceApplicationTests {

	@Test
	void contextLoads() {
	}

}
