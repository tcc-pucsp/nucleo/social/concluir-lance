package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.propriedade.PropriedadeMitigacaoOferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.propriedade.PropriedadeNotificacao;

@EnableConfigurationProperties({PropriedadeMitigacaoOferta.class, PropriedadeNotificacao.class})
@SpringBootApplication
public class ConcluirLanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConcluirLanceApplication.class, args);
	}

}
