package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.observador;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.recurso.LanceRecurso;

@Component
public class LanceObservador implements LanceRecurso {

    private final LanceRecurso lanceRecurso;

    public LanceObservador(@Qualifier("lanceServico") LanceRecurso lanceRecurso) {
        this.lanceRecurso = lanceRecurso;
    }

    @Override
    @RabbitListener(queues = "concluir.lance")
    public void processar(LancePublicacao lancePublicacao) {

        lanceRecurso.processar(lancePublicacao);

    }
}
