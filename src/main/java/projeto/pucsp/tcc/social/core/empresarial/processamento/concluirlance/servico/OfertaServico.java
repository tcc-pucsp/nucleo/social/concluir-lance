package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.servico;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Oferta;

@Component
public class OfertaServico implements OfertaRecurso {

    private final OfertaRecurso ofertaRecurso;

    public OfertaServico(@Qualifier("ofertaRecursoImpl") OfertaRecurso ofertaRecurso) {
        this.ofertaRecurso = ofertaRecurso;
    }

    @Override
    public void publicarOfertaParaMitigacao(Oferta oferta) {
        ofertaRecurso.publicarOfertaParaMitigacao(oferta);
    }
}
