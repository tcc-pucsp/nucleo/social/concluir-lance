package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente;

import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;

public interface NotificacaoRecurso {

    void notificarConclusao(LancePublicacao lancePublicacao);

}
