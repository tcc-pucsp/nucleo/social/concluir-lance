package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Lance;

public interface LanceRepositorio extends JpaRepository<Lance, Integer> {

    @Modifying
    @Transactional
    @Query("update Lance set situacaoLance = :situacaoLance, quantidadeAlimento = quantidadeAlimento - :quantidadeAlimento " +
            "where  situacaoLance <> 2 and id = :id and " +
            "(quantidadeAlimento - :quantidadeAlimento) >= 0")
    Integer atualizarLance(
            @Param("id") Integer id,
            @Param("situacaoLance") Integer situacaoLance,
            @Param("quantidadeAlimento") Integer quantidadeAlimento);

}
