package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Transacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.repositorio.TransacaoRepositorio;

import java.util.Optional;

@Slf4j
@Component
public class TransacaoServico implements LanceRecurso {

    private final TransacaoRepositorio repositorio;

    public TransacaoServico(TransacaoRepositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public void processar(LancePublicacao lancePublicacao) {

        log.info("Comitando transação para {}", lancePublicacao);

        repositorio.save(new Transacao(lancePublicacao));

    }

    Optional<Transacao> obterTransacaoPorLanceId(Integer id){
        return repositorio.findById(id);
    }

}
