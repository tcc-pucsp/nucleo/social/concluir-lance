package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class Oferta {

    private final Integer codigo;

    private final Integer quantidade;

    public Oferta(LancePublicacao lancePublicacao) {
        codigo = lancePublicacao.getOfertaId();
        quantidade = lancePublicacao.getQuantidadeAlimento();
    }
}
