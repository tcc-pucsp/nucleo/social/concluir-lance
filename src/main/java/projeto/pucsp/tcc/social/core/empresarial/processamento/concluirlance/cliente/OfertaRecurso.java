package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente;

import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Oferta;

public interface OfertaRecurso {

    void publicarOfertaParaMitigacao(Oferta oferta);

}
