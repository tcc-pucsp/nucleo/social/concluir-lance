package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.NotificacaoRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.propriedade.PropriedadeNotificacao;

@Slf4j
@Component
public class NotificacaoRecursoImpl implements NotificacaoRecurso {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeNotificacao propriedadeNotificacao;

    public NotificacaoRecursoImpl(RabbitTemplate rabbitTemplate, PropriedadeNotificacao propriedadeNotificacao) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeNotificacao = propriedadeNotificacao;
    }

    @Override
    public void notificarConclusao(LancePublicacao lancePublicacao) {

        log.info("{} publicando para notificacao ", lancePublicacao);

        rabbitTemplate.convertAndSend(propriedadeNotificacao.getTopicExchange(), lancePublicacao);

    }
}
