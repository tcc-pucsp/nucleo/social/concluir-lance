package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Transacao;

public interface TransacaoRepositorio extends JpaRepository<Transacao, Integer> {

}
