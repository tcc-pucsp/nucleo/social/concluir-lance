package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.propriedade;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ToString
@Setter
@Getter
@ConfigurationProperties(prefix = "rabbit.notificacao")
@Validated
public class PropriedadeNotificacao {

    private String topicExchange;

}
