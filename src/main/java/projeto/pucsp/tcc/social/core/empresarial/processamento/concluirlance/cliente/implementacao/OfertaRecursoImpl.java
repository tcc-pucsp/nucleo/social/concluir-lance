package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Oferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.propriedade.PropriedadeMitigacaoOferta;

@Slf4j
@Component
public class OfertaRecursoImpl implements OfertaRecurso {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeMitigacaoOferta propriedadeMitigacaoOferta;

    public OfertaRecursoImpl(RabbitTemplate rabbitTemplate, PropriedadeMitigacaoOferta propriedadeMitigacaoOferta) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeMitigacaoOferta = propriedadeMitigacaoOferta;
    }

    @Override
    public void publicarOfertaParaMitigacao(Oferta oferta) {

        log.info("{} publicando para mitigação ", oferta);

        rabbitTemplate.convertAndSend(propriedadeMitigacaoOferta.getTopicExchange(), oferta);

    }
}
