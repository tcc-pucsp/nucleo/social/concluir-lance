package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.NotificacaoRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.cliente.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.Oferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.repositorio.LanceRepositorio;

@Slf4j
@Service
public class LanceServico implements LanceRecurso {

    private final LanceRepositorio repositorio;

    private final TransacaoServico transacaoServico;

    private final OfertaRecurso ofertaRecurso;

    private final NotificacaoRecurso notificacaoRecurso;

    public LanceServico(
            LanceRepositorio repositorio,
            TransacaoServico transacaoServico,
                        @Qualifier("ofertaServico") OfertaRecurso ofertaRecurso,
            NotificacaoRecurso notificacaoRecurso) {
        this.repositorio = repositorio;
        this.transacaoServico = transacaoServico;
        this.ofertaRecurso = ofertaRecurso;
        this.notificacaoRecurso = notificacaoRecurso;
    }

    @Override
    public void processar(LancePublicacao lancePublicacao) {

        log.info("Pesquisando por {}", lancePublicacao);

        repositorio
                .findById(lancePublicacao.getLanceId())
                .map(lance -> lance.compensar(lancePublicacao.getQuantidadeAlimento()))
                .map(lance -> repositorio.atualizarLance(lance.getId(), lance.getSituacaoLance(), lancePublicacao.getQuantidadeAlimento()))
                .ifPresent(atualizado -> {

                    if (atualizado == 1) {

                        log.info("{} foi compensado com sucesso", lancePublicacao);

                        transacaoServico.processar(lancePublicacao);

                        notificacaoRecurso.notificarConclusao(lancePublicacao);

                    } else if (transacaoServico.obterTransacaoPorLanceId(lancePublicacao.getLanceId()).isEmpty()) {

                        ofertaRecurso.publicarOfertaParaMitigacao(new Oferta(lancePublicacao));

                    } else {

                        log.error(" errorrrrrrrrrrrrrrrr");

                    }

                });
    }

}
