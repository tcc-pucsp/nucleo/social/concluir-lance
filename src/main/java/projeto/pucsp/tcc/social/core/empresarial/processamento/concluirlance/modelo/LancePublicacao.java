package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo;

import lombok.Data;

@Data
public class LancePublicacao {

    private Integer lanceId;

    private Integer ofertaId;

    private Integer socialId;

    private Integer empresarialId;

    private Integer alimentoId;

    private Integer quantidadeAlimento;

}
