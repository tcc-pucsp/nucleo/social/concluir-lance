package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.recurso;

import projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo.LancePublicacao;

public interface LanceRecurso {

    void processar(LancePublicacao lancePublicacao);

}
