package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lance")
@Data
public class Lance {

    private static final Integer LANCE_COMPENSANDO_POR_COMPLETO = 2;

    private static final Integer LANCE_COMPENSANDO_PARCIALMENTE = 1;

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "social_id")
    private Integer socialId;

    @Column(name = "alimento_id")
    private Integer alimentoId;

    @Column(name = "situacao_lance")
    private Integer situacaoLance;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    public Lance compensar(Integer quantidadeAlimento) {

        if (calcularVariacaoCompensamento(quantidadeAlimento) >= 0) {

            situacaoLance = LANCE_COMPENSANDO_POR_COMPLETO;

        } else {

            situacaoLance = LANCE_COMPENSANDO_PARCIALMENTE;

        }

        return this;

    }

    private Integer calcularVariacaoCompensamento(Integer quantidadeAlimento) {
        return quantidadeAlimento - this.quantidadeAlimento;
    }

}
