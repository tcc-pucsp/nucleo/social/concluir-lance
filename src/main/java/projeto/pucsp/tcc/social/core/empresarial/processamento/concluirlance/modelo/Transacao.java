package projeto.pucsp.tcc.social.core.empresarial.processamento.concluirlance.modelo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "transacao")
@Data
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "lance_fk")
    private Integer lanceFk;

    @Column(name = "oferta_fk")
    private Integer ofertaFk;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    public Transacao(LancePublicacao lancePublicacao) {
        lanceFk = lancePublicacao.getLanceId();
        ofertaFk = lancePublicacao.getOfertaId();
        quantidadeAlimento = lancePublicacao.getQuantidadeAlimento();
    }

}
